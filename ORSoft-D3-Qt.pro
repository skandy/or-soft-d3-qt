#-------------------------------------------------
#
# Project created by QtCreator 2015-02-10T19:15:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ORSoft-D3-Qt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
QT += webkit
QT += webkitwidgets
