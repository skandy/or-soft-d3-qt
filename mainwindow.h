#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QTextDocumentWriter>
#include <QTextStream>
#include <QUrl>

// This ist the path to the temporary stored csv for the web-sources it has to be set in the html of HTML_PATH
#define TEMP_FILE_PATH "/Users/skandy/workspace/ORSOFT-D3/tmp.csv"
// Path to the diagramm-html
//#define HTML_PATH "file://$DIR/../../test3.html"
#define HTML_PATH "file:///Users/skandy/workspace/ORSOFT-D3/test4.html"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QString* mCsvData1;
    QString* mCsvData2;
    QList<QPair<QString,int> > mSeqCountList;
    QList<QPair<QString,int> > mSeqCountList2;


    QString getHTMLPath();
    QString prepareCsv(QString s, int filenr);
    QString prepareCsv(QTextStream* in, int filenr);
    void saveTempFile();
    void seperateSeqList(QTextStream* in, int fileNr);
    int getSeqCount(int fileNr, QString line);

private slots:
    void openFiles(QString* seqCntFilename = NULL, QString* fileName = NULL, QString* seqCntFilename2 = NULL, QString * fileName2 = NULL);
};

#endif // MAINWINDOW_H
