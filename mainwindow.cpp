#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(openFiles()));
    ui->webView->settings()->setObjectCacheCapacities(0,0,0);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openFiles(QString* seqCntFilename, QString* fileName, QString* seqCntFilename2, QString * fileName2)
{



    if(!fileName)
    {


        fileName = new QString(QFileDialog::getOpenFileName(this, tr("Open first File"),
                                                     "",
                                                     tr("Files (*.*)")));
    }

    if(!seqCntFilename)
    {


        seqCntFilename = new QString(QFileDialog::getOpenFileName(this, tr("Open first seqCntFile"),
                                                     "",
                                                     tr("Files (*.*)")));
    }


    if(!fileName2)
    {
        fileName2 = new QString(QFileDialog::getOpenFileName(this, tr("Open second File"),
                                                     "",
                                                     tr("Files (*.*)")));
    }

    if(!seqCntFilename2)
    {


        seqCntFilename2 = new QString(QFileDialog::getOpenFileName(this, tr("Open second seqCntFile"),
                                                     "",
                                                     tr("Files (*.*)")));
    }

    /*if(mCsvData1)
    {
        delete mCsvData1;
    }

    if(mCsvData2)
    {
        delete mCsvData2;
    }*/



    QFile file(*fileName);
    QFile file2(*fileName2);
    QFile seqCntFile(*seqCntFilename);
    QFile seqCntFile2(*seqCntFilename2);


    file.open(QIODevice::ReadOnly);
    file2.open(QIODevice::ReadOnly);
    seqCntFile.open(QIODevice::ReadOnly);
    seqCntFile2.open(QIODevice::ReadOnly);


    QTextStream in(&file);
    QTextStream in2(&file2);
    QTextStream in3(&seqCntFile);
    QTextStream in4(&seqCntFile2);


//    mCsvData1 = new QString(prepareCsv(in.readAll(),1));
//    mCsvData2 = new QString(prepareCsv(in2.readAll(),2));

    seperateSeqList(&in3,1);
    seperateSeqList(&in4,2);
    mCsvData1 = new QString(prepareCsv(&in,1));
    mCsvData2 = new QString(prepareCsv(&in2,2));
    file.close();
    file2.close();
    seqCntFile.close();
    seqCntFile2.close();

    saveTempFile();

    qDebug()<<getHTMLPath();
    ui->webView->load(QUrl(getHTMLPath()));

    delete fileName;

}

void MainWindow::saveTempFile()
{
    QFile file (TEMP_FILE_PATH);
    file.open(QIODevice::WriteOnly);
    QTextStream outstream(&file);
    outstream << *mCsvData1;
    outstream << *mCsvData2;
    file.close();
}

QString MainWindow::prepareCsv(QString s, int filenr)
{

    return s.replace(";",",").replace("\n",",fileNr"+QString::number(filenr)+"\n");
}

QString MainWindow::prepareCsv(QTextStream* in, int filenr)
{
    QString result;
    QString line;
    int lineNr = 1;
    while(!in->atEnd())
    {
            line = in->readLine();
            if(filenr>1 && lineNr == 1)
            {
                lineNr++;
                qDebug()<<filenr << ":"<< lineNr << " skipped";
                continue;
            }
            result += line+ "," + (filenr == 1 && lineNr == 1?"sourceIdentifier":QString::number(lineNr*1000+filenr)) + "," + (filenr == 1 && lineNr==1?"seqcount":(filenr == 1?QString::number(getSeqCount(1,line)):QString::number(getSeqCount(1,line))))+ "\n";
            lineNr++;
            qDebug()<<filenr << ":"<< lineNr;
    }
    return result.replace(";",",").replace("\n",",fileNr"+QString::number(filenr)+"\n");
}

QString MainWindow::getHTMLPath()
{
    return QString(HTML_PATH).replace("$DIR",QDir::currentPath());
}

void MainWindow::seperateSeqList(QTextStream* in, int fileNr)
{
    if(fileNr == 1)
    {
        mSeqCountList.clear();
    }
    else
    {
        mSeqCountList2.clear();
    }
    while(!in->atEnd())
    {
        QString line = in->readLine();
        QPair<QString,int> pair;
        pair.first = line.split(";").first();
        pair.second = line.split(";").at(1).toInt();

        if(fileNr == 1)
        {
            mSeqCountList.append(pair);
        }
        else
        {
            mSeqCountList2.append(pair);
        }
    }
}

int MainWindow::getSeqCount(int fileNr, QString line)
{
    int resCounter = 0;
    for(int i = 0; i<(fileNr==1?mSeqCountList.size():mSeqCountList2.size());i++)
    {
        QString obj = line.split(";").at(3);
        if(fileNr==1?mSeqCountList.at(i).first.contains(obj):mSeqCountList2.at(i).first.contains(obj))
        {
            resCounter += fileNr==1?mSeqCountList2.at(i).second:mSeqCountList2.at(i).second;
            //return fileNr==1?mSeqCountList2.at(i).second:mSeqCountList2.at(i).second;
        }
    }
    return resCounter;
}
