How To
=====

1. You need to Download the web-sources at first located right [here](https://bitbucket.org/skandy/or-soft-d3)
2. open _mainwindow.cpp_ within this project and edit _TEMP_FILE_PATH_ and _HTML_FILE_PATH_
3. open the html and also set the same path to the same temp-csv
4. build and run the project

Quickstart
=====
You'll find a the whole project ready to use under mac os and windows in the sample-folder of this project.

If you need any help with this little project please feel free to contact me.